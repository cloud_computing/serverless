const AWS = require("aws-sdk");
const dynamo = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event, context) => {
  let body;
  let statusCode = 200;
  const headers = {
    "Content-Type": "application/json"
  };

  try {
    switch (event.httpMethod) {      
      case "GET":
        if (event.pathParameters != null) {
            body = await dynamo
              .get({
                TableName: "products",
                Key: {
                  product_id: event.pathParameters.product_id
                }
              })
              .promise();
        } else {
            body = await dynamo.scan({ TableName: "products" }).promise();
        }
        break;  
      case "POST":
            let requestJSON = JSON.parse(event.body);
            await dynamo
              .put({
                TableName: "products",
                Item: {
                  product_id: requestJSON.product_id,
                  name: requestJSON.name,
                  price: requestJSON.price,                  
                  description: requestJSON.description,
                }
              })
              .promise();
            body = `Added/Updated product ${requestJSON.product_id}`;
            break;  
        case "DELETE":
            await dynamo
                  .delete({
                    TableName: "products",
                    Key: {
                      product_id: event.pathParameters.product_id
                    }
                  })
                  .promise();
                body = `Deleted product ${event.pathParameters.product_id}`;
                break;
      default:
        throw new Error(`Unsupported route: "${event.httpMethod}"`);  
    }
  } catch (err) {
    statusCode = 400;
    body = err.message;
  } finally {
    body = JSON.stringify(body);
  }

  return {
    statusCode,
    body,
    headers
  };
};